<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('customer_id');
            $table->String('store_id');
            $table->integer('reference_num');
            $table->text('delivery_note');
            $table->boolean('order_status')->default(0);
            $table->boolean('payment_status')->default(0);
            $table->unsignedBigInteger('product_id')->unsigned()->index()->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
