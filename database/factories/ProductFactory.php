<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'product_code' => $faker->words(1,  true),
        'product_name' => $faker->words( 2, true),
        'description' => $faker->text(50),
        'stock' => $faker->randomNumber(2),
        'cost' => $faker->randomNumber(2),
        'created_at' => $faker->dateTimeBetween('-10 months'),
        'updated_at' => $faker->dateTimeBetween('-5 months', 'now', null),
    ];
});
