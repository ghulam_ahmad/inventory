
require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue'
import vSelect from 'vue-select'
import $ from "jquery";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
import 'vue-select/dist/vue-select.css';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


Vue.component('v-select', vSelect)
Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('order-form', require('./components/create-order-form.vue').default);
Vue.component('update-order-form', require('./components/update-order-form.vue').default);
Vue.component('vue-table', require('./components/vue-table.vue').default);

new Vue({
    el: '#app',
});
