
@extends('layouts.layout')

@section('content')

    <div class="container">
      <div class="row">
        <div style="padding-top: 20px; " class="col-md-6 automargin">
          <h2>Order Information</h2>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->


<div class="container">
  <div class="row">
    <div class="right">
        <div id="content" style="padding: 20px;">
               <div style="float: right;">
                    <a href="/orders"><button type="button" class="btn btn-primary">Show All Orders</button></a>
                    <a href="/orders/{{ $order->id }}/edit"><button type="button" class="btn btn-primary">Edit Order</button></a>
               </div>
       </div>
    </div>
  </div>
</div>



	<div id="page" class="container">
		

            <div class="row" style="padding-top: 30px">
              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="far fa-user"></i></span>

                     <div class="info-box-content">
                      <span class="info-box-text">Customer Name</span>
                      <span class="info-box-number">
                       {{ $order->customer->name }}
                      </span>
                    </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>

                   <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="fas fa-at"></i></span>

                     <div class="info-box-content">
                      <span class="info-box-text">Customer Email</span>
                      <span class="info-box-number">
                       {{ $order->customer->email }}
                      </span>
                    </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>

              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="fas fa-hashtag"></i></span>

                     <div class="info-box-content">
                      <span class="info-box-text">Reference Number</span>
                      <span class="info-box-number">
                       {{ $order->reference_num }}
                      </span>
                    </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>

              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="fas fa-shopping-bag"></i></span>

                     <div class="info-box-content">
                      <span class="info-box-text">Order Status</span>
                      <span class="info-box-number">
                       {{ $order->order_status === 1 ? 'Ready' : 'Unprepared' }}
                      </span>
                    </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
            </div>

            <div class="row">
              <div style="padding: 10px">
                <label>Order Booking Date/Time</label>
                <h5>{{ $order->created_at }}</h5>
              </div>
            </div>


                    <div class="container">
                      <h2>Products</h2>
                         <table class="table table-striped">
                              <thead>
                                   <tr>
                                        <th>product Code</th>
                                        <th>product Name</th>
                                        <th>product Quantity</th>
                                        <th>product Cost</th>

                                   </tr>
                              </thead>
                    @foreach ($order->products as $product)
                              <tbody>
                                   <tr>
                                        <th>{{ $product->product_code}}</th>
                                        <th>{{ $product->product_name}}</th>
                                        <th>{{ $product->pivot->quantity}}</th>
                                        <th>{{ $product->pivot->cost}}</th>
                                   </tr>
                              </tbody>
                    @endforeach
                    </table>
               </div>

               <div class="container">
                 <div class="row">
                  <div style="padding: 10px">
                    <label>Delivery Note</label>
                    <p>{{ $order->delivery_note }}</p>
                  </div>
                 </div>
               </div>
	</div>


    
@endsection