@extends('layouts.layout')


@section('content')

<style>
    span.select2-selection.select2-selection--single{
        height: 100%;
        margin-bottom: 10px;
    }

    span.select2-selection__arrow{
        height: 70% !important;
    }

    span.select2.select2-container.select2-container--default.select2-container--focus{
        width: 100% !important;
    }
</style>

<div class="container" style="padding-top: 20px; padding-bottom: 20px;">
  <h1>New Order</h1>
  <div id="app">
        <order-form></order-form> 
  </div>
</div>
<script type="text/javascript">


  function remove(id){
    $('#product_' + id).remove();
    Swal.fire(
  'Product Deleted',
  '',
  'success'
)
  }
</script>
@endsection