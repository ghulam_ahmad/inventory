@extends('layouts.layout')

@section('content')

<style>
     .img img {
      width: 100px;
      display: block;
      height: auto;
     }
     .center{
      padding: 20px;
     }

     .right{
      float: right;
     }
     .no-decoration:hover{
        text-decoration: none;
     }
     .automargin {
        margin: auto;
     }

</style>

    <div class="container">
      <div class="row">
        <div class="col-md-6 automargin">
          <h2><a class="no-decoration" href="/orders">Orders</a></h2>
        </div><!-- /.col -->
        <div class="col-md-6 center">
            <a href="/orders/create"><button type="button" class="btn btn-primary right">Add New Order</button></a>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->

   <div class="container">
     <form action="/orders" method="GET" role="search">
         <div class="input-group">
             <input type="text" class="form-control" name="search"
                 placeholder="Search Orders"> <span class="input-group-btn">
                 <button type="submit" class="btn btn-primary">
                     <span class="glyphicon glyphicon-search">Search</span>
                 </button>
             </span>
         </div>
     </form>
</div>
<div class="container">
     @if(isset($orders))

         <table class="table table-hover">
             <thead>
                 <tr>
                     <th>Reference Number</th>
                     <th>Customer Name</th>
                     <th>Customer Email</th>
                     <th>Order Status</th>
                     <th>Payment Status</th>
                     <th>Store Name</th>
                     <th>Created At</th>
                 </tr>
             </thead>
             <tbody>
              
                 @foreach($orders as $order)
                 <tr>
                     <td><a href="/orders/{{ $order->id }}">{{ $order->reference_num }}</a></td>
                     <td><a href="/customers/{{ $order->customer->id }}">{{ $order->customer->name }}</a></td>
                     <td>{{ $order->customer->email ?? '' }}</td>
                     <td><h6><b-badge variant="{{ $order->order_status == 1 ? 'success' : '' }}">{{ $order->order_status == 1 ? 'Ready' : 'Unprepared' }}</b-badge></h6></td>
                     <td><h6><b-badge variant="{{ $order->payment_status == 1 ? 'success' : 'danger' }}">{{ $order->payment_status == 1 ? 'Approved' : 'Unapproved' }}</b-badge></h6></td>
                     <td><a href="/stores/{{ $order->store->id }}">{{ $order->store->name }}</a></td>
                     <td>{{ $order->created_at }}</td>
                     <td><a href="/orders/{{ $order->id }}/edit"><button type="button" class="btn btn-primary"><i class="fas fa-edit"></i></button></button></a></td>
                  <td><a href="delete/orders/{{ $order->id }}"><button type="button" class="btn btn-danger"><i class="fas fa-trash"></i></button></a></td>
                </tr>
                 @endforeach

             </tbody>
         </table>
         @endif
     </div>

@endsection
