@extends('layouts.layout')

@section('content')
<div class="container" style="padding-top: 20px; padding-bottom: 20px;">
     <h1>Edit Store</h1>
     <form method="POST" action="/stores/{{ $store->id }}">
          @csrf
          @method('PUT')
          <div class="form-group">
            <label for="exampleFormControlInput1">Store Name</label>
          <input style="@error('name') border: 2px solid #ff3860; @enderror" type="text" name="name" class="form-control" id="exampleFormControlInput1" placeholder="Enter Store Name" value="{{ $store->name }}">
          @error('name')
            <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('name') }}</p>
            @enderror 
          
          </div>
          
          <div>
               <button type="submit" class="btn btn-primary">Submit</button>
             </div>
        </form>
</div>
@endsection