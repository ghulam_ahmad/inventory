@extends('layouts.layout')

@section('content')
<style>
     .img img {
      width: 100px;
      display: block;
      height: auto;
     }
     .center{
      padding: 20px;
     }

     .right{
      float: right;
     }
     .no-decoration:hover{
        text-decoration: none;
     }
     .automargin {
        margin: auto;
     }

</style>

    <div class="container">
      <div class="row">
        <div class="col-md-6 automargin">
          <h2><a class="no-decoration" href="/stores">Stores</a></h2>
        </div><!-- /.col -->
        <div class="col-md-6 center">
            <a href="/stores/create"><button type="button" class="btn btn-primary right">Add New Store</button></a>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->

   <div class="container">
    <form action="/stores" method="GET" role="search">
        <div class="input-group">
            <input type="text" class="form-control" name="search"
                placeholder="Search Stores"> <span class="input-group-btn">
                <button type="submit" class="btn btn-primary">
                    <span class="glyphicon glyphicon-search">Search</span>
                </button>
            </span>
        </div>
    </form>
</div>
<div class="container">
     @if(isset($stores))
    
         <table class="table table-hover">
             <thead>
                 <tr>
                     <th>Store Name</th>
                     <th>Created At</th>
                     <th></th>
                     <th></th>
                 </tr>
             </thead>
             <tbody>
                 @foreach($stores as $store)
                 <tr>
                     <td><a href="/stores/{{ $store->id }}">{{$store->name}}</a></td>
                     <td>{{$store->created_at}}</td>
                     <td style="float: right;" ><a href="/stores/{{ $store->id }}/edit"><button type="button" class="btn btn-primary"><i class="fas fa-edit"></i></button></a></td>
                     <td><a href="delete/stores/{{ $store->id }}"><button type="button" class="btn btn-danger"><i class="fas fa-trash"></i></button></a></td>
                    </tr>
                 @endforeach
  
             </tbody>
         </table>
         @endif
     </div>

@endsection
     