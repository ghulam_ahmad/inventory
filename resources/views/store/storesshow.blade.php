
@extends('layouts.layout')

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-4">
      
    </div>
    <div class="col-md-4">
            <div class="info-box">
              <div class="info-box-content text-center">
               <span class="info-box-text">Store Name</span>
               <h3 class="info-box-text">{{ $store->name }}</h3>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>

          <div class="col-md-4">
            
          </div>

      

  </div>
</div>
              
                

	<div id="page" class="container">               

       <div class="float-right p-2">
             <a href="/stores"><button type="button" class="btn btn-primary">Show All Stores</button></a>
          <a href="/stores/{{ $store->id }}/edit"><button type="button" class="btn btn-primary">Edit Store</button></a>
          </div>

  @if(isset($store->products))
  <h4 class="p-2">Products in {{ $store->name }}</h4>
      <table class="table table-hover">
          <thead>
              <tr>
                  <th>Product Code</th>
                  <th>Product Name</th>
                  <th>Description</th>
                  <th>Stock</th>
                  <th>Cost</th>
                  <th>Created at</th>
              </tr>
          </thead>
          <tbody>
              @foreach($store->products as $product)
              <tr>
                  <td><a href="/products/{{ $product->id }}">{{$product->product_code}}</a></td>
                  <td><a href="/products/{{ $product->id }}">{{$product->product_name}}</a></td>
                  <td>{{$product->description}}</td>
                  <td>{{$product->stock}}</td>
                  <td>{{$product->cost}}</td>
                  <td>{{$product->created_at}}</td>
              </tr>
              @endforeach

          </tbody>
      </table>
   @endif



      @if(isset($store->orders))
  <h4 class="p-2">Orders in {{ $store->name }}</h4>
      <table class="table table-hover">
          <thead>
              <tr>
                  <th>Reference Number</th>
                  <th>Customer Name</th>
                  <th>Order Status</th>
                  <th>Payment Status</th>
                  <th>Order Date</th>
              </tr>
          </thead>
          <tbody>
              @foreach($store->orders as $order)
              <tr>
                  <td><a href="/orders/{{ $order->id }}">{{ $order->reference_num }}</a></td>
                  <td><a href="/customers/{{ $order->customer->id }}">{{ $order->customer->name }}</a></td>
                  <td>{{ $order->order_status == 1 ? 'Ready' : 'Unprepared' }}</td>
                  <td>{{ $order->payment_status == 1 ? 'Approved' : 'Not Approved' }}</td>
                  <td>{{ $order->created_at }}</td>
              </tr>
              @endforeach

          </tbody>
      </table>
   @endif

	</div>
    
@endsection