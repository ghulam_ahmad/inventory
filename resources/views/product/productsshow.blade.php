@extends('layouts.layout')

@section('content')

    <div class="container">
      <div class="row">
        <div style="padding-top: 20px; " class="col-md-6 automargin">
          <h2>Product Information</h2>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->


<div class="container">
  <div class="row">
    <div class="right">
        <div id="content" style="padding: 20px;">
               <div style="float: right;">
                    <a href="/products"><button type="button" class="btn btn-primary">Show All product</button></a>
                    <a href="/products/{{ $product->id }}/edit"><button type="button" class="btn btn-primary">Edit product</button></a>
               </div>
       </div>
    </div>
  </div>
</div>

	<div id="page" class="container">
        

               <!-- slider -->
               <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                         @foreach ($product->getMedia('images') as $key => $image)
                              <div class="carousel-item {{ $key == 0 ? 'active' : ''}}">
                                   <img class="d-block w-100" src="{{$image->getFullUrl()}}">
                              </div>
                         @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>


   <div class="row" style="padding-top: 30px">
              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="far fa-user"></i></span>

                     <div class="info-box-content">
                      <span class="info-box-text">Product Primary Code</span>
                      <h4 class="info-box-number">
                       {{ $product->product_code }}
                      </h4>
                    </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>

                   <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="fas fa-at"></i></span>

                     <div class="info-box-content">
                      <span class="info-box-text">Product Name</span>
                      <h4 class="info-box-number">
                       {{ $product->product_name }}
                      </h4>
                    </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>

              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="fas fa-hashtag"></i></span>

                     <div class="info-box-content">
                      <span class="info-box-text">Stock Quantity</span>
                      <h4 class="info-box-number">
                       {{ $product->cost }}
                      </h4>
                    </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>

              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="fas fa-shopping-bag"></i></span>

                     <div class="info-box-content">
                      <span class="info-box-text">Cost</span>
                      <h4 class="info-box-number">
                       {{ $product->cost }}
                      </h4>
                    </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
            </div>

                <div class="container" style="padding-top: 20px">
                  <h3>Secondary Codes</h3>
                         <table class="table table-striped">
                              <thead>
                                   <tr>
                                        <th>Store Name</th>
                                        <th>Store Code</th>
                                   </tr>
                              </thead>
                    @foreach ($product->stores as $store)
                              <tbody>
                                   <tr>
                                        <th>{{ $store->name}}</th>
                                        <th>{{ $store->pivot->code }}</th>
                                   </tr>
                              </tbody>
                    @endforeach
                    </table>
               </div>

              <div class="row">
                <div style="padding: 10px">
                    <label for="description">Description</label>
                      <p>{{ $product->description }}</p>
                </div>
              </div>

          </div>
    
@endsection