@extends('layouts.layout')

@section('content')
<div class="container" style="padding-top: 20px; padding-bottom: 20px;">
     <h1>Edit Product</h1>
     <form method="POST" action="/products/{{ $product->id }}">
          @csrf
          @method('PUT')
          <div class="form-group">
            <label for="exampleFormControlInput1">Product Code</label>
          <input style="@error('product_code') border: 2px solid #ff3860; @enderror" type="text" name="product_code" class="form-control" id="exampleFormControlInput1" placeholder="Enter Product Code" value="{{ $product->product_code }}">
          @error('product_code')
            <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('product_code') }}</p>
            @enderror 
          
          </div>
          <div class="form-group">
               <label for="exampleFormControlInput1">Product Name</label>
               <input style="@error('product_name') border: 2px solid #ff3860; @enderror"  type="text" name="product_name" class="form-control" id="exampleFormControlInput1" placeholder="Enter Product Name" value="{{ $product->product_name }}">
               @error('product_name')
               <p style="color: #ff3860; font-size: 12px; padding: 5px;">{{ $errors->first('product_name') }}</p>
               @enderror
             </div>
         
             <div class="form-group">
               <label for="exampleFormControlInput1">Stock</label>
               <input style="@error('stock') border: 2px solid #ff3860; @enderror"  type="text" name="stock" class="form-control" id="exampleFormControlInput1" placeholder="Enter Stock Quantity" value="{{ $product->stock }}">
               @error('stock')
               <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('stock') }}</p>
               @enderror
             </div>
             <div class="form-group">
               <label for="exampleFormControlInput1">Cost</label>
               <input style="@error('cost') border: 2px solid #ff3860; @enderror"  type="text" name="cost" class="form-control" id="exampleFormControlInput1" placeholder="Enter Cost Price" value="{{ $product->cost }}">
               @error('cost')
               <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('cost') }}</p>
               @enderror
             </div>

          <h3>Stores</h3>
          <hr>
            @foreach ($product->stores as $store)
            <div class="form-group">
              <div class="row">
                  <div class="col-md-3" >
                  <label >{{ $store->name }}</label>
              </div>
              <div class="col-md-6">
                  <input  type="text" name="code-{{ $store->id }}" value="{{ $store->pivot->code }}" class="form-control" id="exampleFormControlInput1" placeholder="Enter product Code for {{ $store->name }}">
              </div>
              <div class="col-md-3"></div>
              </div>
               @error('code-' . $store->id)
               <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $message }}</p>
               @enderror
             </div>
            @endforeach
            <hr>


             <div class="form-group">
               <label for="exampleFormControlTextarea1">Description</label>
               <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3">{{ $product->description }}</textarea>
             </div>
            @foreach($product->getMedia('images') as $image)
              <div><img src="{{$image->getFullUrl()}}" alt=""></div>
            @endforeach
             <div class="col-md-12">
             <input type="file" class="form-control" id="images" name="image" onchange="preview_images();" value="{{$product->getMedia()}}"/>
          </div>
       
            <div class="row" id="image_preview"></div>
       

          <div>
               <button type="submit" name='submit_image' class="btn btn-primary">Submit</button>
             </div>
        </form>
</div>
@endsection
