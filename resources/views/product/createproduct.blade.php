@extends('layouts.layout')

@section('content')


<div class="container" style="padding-top: 20px; padding-bottom: 20px;">
     <h1>New Product</h1>
     <form method="POST" action="/products" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            <label for="exampleFormControlInput1">Product Code</label>
            <input value="{{ old('product_code') }}" style="@error('product_code') border: 2px solid #ff3860; @enderror"  type="text" name="product_code" class="form-control" id="exampleFormControlInput1" placeholder="Enter Product Code">
            @error('product_code')
            <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('product_code') }}</p>
            @enderror

          </div>
          <div class="form-group">
               <label for="exampleFormControlInput1">Product Name</label>
               <input value="{{ old('product_name') }}" style="@error('product_name') border: 2px solid #ff3860; @enderror"  type="text" name="product_name" class="form-control" id="exampleFormControlInput1" placeholder="Enter Product Name">
               @error('product_name')
               <p style="color: #ff3860; font-size: 12px; padding: 5px;">{{ $errors->first('product_name') }}</p>
               @enderror
             </div>
         
             <div class="form-group">
               <label for="exampleFormControlInput1">Stock</label>
               <input value="{{ old('stock') }}" style="@error('stock') border: 2px solid #ff3860; @enderror"  type="number" name="stock" class="form-control" id="exampleFormControlInput1" placeholder="Enter Stock Quantity">
               @error('stock')
               <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('stock') }}</p>
               @enderror
             </div>
             <div class="form-group">
               <label for="exampleFormControlInput1">Cost</label>
               <input value="{{ old('cost') }}" style="@error('cost') border: 2px solid #ff3860; @enderror"  type="number" name="cost" class="form-control" id="exampleFormControlInput1" placeholder="Enter Cost Price">
               @error('cost')
               <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('cost') }}</p>
               @enderror
             </div>

          <h3>Stores</h3>
          <hr>
            @foreach ($stores as $store)
            <div class="form-group">
              <div class="row">
                  <div class="col-md-3" >
                  <label >{{ $store->name }}</label>
              </div>
              <div class="col-md-6">
                  <input  type="text" name="code-{{ $store->id }}" class="form-control" id="exampleFormControlInput1" placeholder="Enter product Code for {{ $store->name }}">
              </div>
              <div class="col-md-3"></div>
              </div>
               @error('code-' . $store->id)
               <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $message }}</p>
               @enderror
             </div>
            @endforeach
            <hr>

             <div class="form-group">
               <label for="exampleFormControlTextarea1">Description</label>
               <textarea class="form-control" style="@error('description') border: 2px solid #ff3860; @enderror" rows = "7" cols = "5" name="description" id="exampleFormControlTextarea1" rows="3">{{ old('description') }}</textarea>
               @error('description')
               <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('description') }}</p>
               @enderror
             </div>

             <div class="col-md-12">
              <input type="file" class="form-control" id="images" name="images[]" onchange="preview_images();" multiple/>
          </div>
        
        <div id="image_preview">
              <b-container fluid class="p-4 bg-light">
                <b-row>
                  <b-col>
                    <b-img thumbnail fluid></b-img>
                  </b-col>
                </b-row>
              </b-container>          
        </div>    

          <div style="padding-top: 20px;">
               <button type="submit" name='submit_image' class="btn btn-primary">Submit</button>
             </div>
        </form>


</div>
@endsection
