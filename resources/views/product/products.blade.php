@extends('layouts.layout')

@section('content')

    <div class="container">
      <div class="row">
        <div class="col-md-6 automargin">
          <h2><a class="no-decoration" href="/products">Products</a></h2>
        </div><!-- /.col -->
        <div class="col-md-6 center">
            <a href="/products/create"><button type="button" class="btn btn-primary right">Add New product</button></a>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  <!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="container">
            <form action="/products" method="GET" role="search">
                <div class="input-group">
                    <input type="text" class="form-control" name="search"
                        placeholder="Search Products"> 
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-primary">
                            <span class="glyphicon glyphicon-search">Search</span>
                        </button>
                    </span>
                </div>
            </form>
            <br>
       </div>
       <div class="container-fluid">
                <vue-table></vue-table>
        </div>
       
       </div>
    </div><!-- /.container-fluid -->
  </div>


@endsection
     
