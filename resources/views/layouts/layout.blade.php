<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="stylesheet" href="./plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <link rel="stylesheet" href="/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="/plugins/jqvmap/jqvmap.min.css">
  <link rel="stylesheet" href="/css/adminlte.min.css">
  <link rel="stylesheet" href="/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="/plugins/summernote/summernote-bs4.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <title>Inventory</title>

  <link rel="stylesheet" href="/css/adminlte.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="/css/style.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="https://unpkg.com/vue@latest"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <script src="https://unpkg.com/vue-select@latest"></script>
  <link rel="stylesheet" href="https://unpkg.com/vue-select@latest/dist/vue-select.css">
  <script src="https://unpkg.com/vue-select@3.0.0"></script>
  <link rel="stylesheet" href="https://unpkg.com/vue-select@3.0.0/dist/vue-select.css">
  <script src="https://kit.fontawesome.com/8d9c73bef7.js" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script type="text/javascript" src="http://www.expertphp.in/js/jquery.form.js"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>

  <script>
    function preview_images()
    {
     var total_file=document.getElementById("images").files.length;
     for(var i=0;i<total_file;i++)
     {
      $('#image_preview').append("<div class='col-md-3'><img class='img-responsive' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
     }
    }
    </script>

</head>

<body class="hold-transition sidebar-mini sidebar-collapse">
<div class="wrapper" id="app">
<!--------------------------------------------------------------------------->

<!-- Navbar -->
  <nav class="main-header navbar navbar-expanded navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
         <li class="nav-item d-none d-sm-inline-block">
        <div class="textcolor">
          <i class="fas fa-user" style="padding-right: 5px;"></i>
          {{ Auth::user()->name }}
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->
<!---------------------------------------------------------------------------->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary">
    <!-- Brand Logo -->
    <a href="/products" class="brand-link">
        <img src="/download.png"
             alt="AdminLTE Logo"
             class="brand-image img-circle elevation-3"
             style="opacity: .8">
      <span class="brand-text font-weight-light">Inventory</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
            <a href="/stats" class="nav-link" style="{{ Request::path() === 'stats' ? 'background-color: green;' : '' }} ">
              <i class="nav-icon fas fa-chart-line"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>


          <li class="nav-item">
            <a href="/products" class="nav-link" style="{{ Request::path() === 'products' ? 'background-color: green;' : '' }} {{ Request::path() === 'products/create' ? 'background-color: green;' : '' }} {{ Request::path() === 'productsshow' ? 'background-color: green;' : '' }} ">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Products
              </p>
            </a>
          </li>

             <li class="nav-item">
              <a href="/orders" class="nav-link" style="{{ Request::path() === 'orders' ? 'background-color: green;' : '' }}; {{ Request::path() === 'orders/create' ? 'background-color: green;' : '' }} ">
                <i class="nav-icon fas fa-truck"></i>
                <p>
                  Orders
                </p>
              </a>
            </li>

            <li class="nav-item">
              <a href="/customers" class="nav-link" style="{{ Request::path() === 'customers' ? 'background-color: green;' : '' }}; {{ Request::path() === 'customers/create' ? 'background-color: green;' : '' }}; {{ Request::path() === 'customers/{$id}' ? 'background-color: green;' : '' }} ">
                <i class="nav-icon fas fa-users"></i>
                <p>
                  Customers
                </p>
              </a>
            </li>

              <li class="nav-item">
               <a href="/stores" class="nav-link" style="{{ Request::path() === 'stores' ? 'background-color: green;' : '' }}; {{ Request::path() === 'stores/create' ? 'background-color: green;' : '' }};  ">
                 <i class="nav-icon fas fa-store"></i>
                 <p>
                   Stores
                 </p>
               </a>
             </li>

            <li class="nav-item">
               <a class="nav-link" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();" style="">
                      <i class="nav-icon fas fa-sign-out-alt"></i>
                          <p>{{ __('Logout') }}</p>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
             </li>
          </ul>
      </nav>
    </div>
  </aside>

  <!-- Content Wrapper. Contains page content -->
    <section class="content-wrapper" style="background-color: white;">
      <div class="container-fluid">
        <div class="row">

              @yield('content')

        </div>
      </div>
    </section>

  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      innventory
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2020.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- Bootstrap 4 -->
<script src="/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://unpkg.com/vue-select@latest"></script>
<link rel="stylesheet" href="https://unpkg.com/vue-select@latest/dist/vue-select.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

<!-- AdminLTE App -->
<script src="/js/adminlte.min.js"></script>
</body>
</html>
