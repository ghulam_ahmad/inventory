@extends('layouts.layout')

@section('content')


    <div class="container">
      <div class="row">
        <div class="col-md- automargin">
          <h2 class="p-3">All Unprepared Orders</h2>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  <!-- /.content-header -->

  <!-- Main content -->

       <div class="container">
            @if(isset($unprepared))
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Reference Number</th>
                            <th>Customer Name</th>
                            <th>Customer Email</th>
                            <th>Store Name</th>
                            <th>Order Status</th>
                            <th>Payment Status</th>
                            <th>Booking Date/Time</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($unprepared as $unpreparedorders)
                        <tr>
                            <td><a href="/orders/{{ $unpreparedorders->id }}">{{ $unpreparedorders->reference_num }}</td>
                            <td><a href="/customers/{{ $unpreparedorders->customer->id }}">{{ $unpreparedorders->customer->name }}</a></td>
                            <td>{{ $unpreparedorders->customer->email }}</td>
                          	<td><a href="/stores/{{ $unpreparedorders->store->id }}">{{ $unpreparedorders->store->name }}</a></td>
                            <td>{{ $unpreparedorders->order_status == 1 ? 'Ready' : 'Unprepared' }}</td>
                            <td>{{ $unpreparedorders->payment_status == 1 ? 'Approved' : 'Unapproved' }}</td>
                            <td>{{ $unpreparedorders->created_at }}</td>
                          
                        </tr>
                        @endforeach
         
                    </tbody>
                </table>
               @endif
            </div>
       
       </div>
    </div><!-- /.container-fluid -->
  </div>


@endsection
     