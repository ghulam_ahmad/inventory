@extends('layouts.layout')

@section('content')


    <div class="container">
      <div class="row">
        <div class="col-md- automargin">
          <h2 class="p-3">All Approved Orders</h2>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  <!-- /.content-header -->

  <!-- Main content -->

       <div class="container">
            @if(isset($approved))
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Reference Number</th>
                            <th>Customer Name</th>
                            <th>Customer Email</th>
                            <th>Store Name</th>
                            <th>Order Status</th>
                            <th>Payment Status</th>
                            <th>Booking Date/Time</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($approved as $approvedorders)
                        <tr>
                            <td><a href="/orders/{{ $approvedorders->id }}">{{ $approvedorders->reference_num }}</td>
                            <td><a href="/customers/{{ $approvedorders->customer->id }}">{{ $approvedorders->customer->name }}</a></td>
                            <td>{{ $approvedorders->customer->email }}</td>
                            <td><a href="/stores/{{ $approvedorders->store->id }}">{{ $approvedorders->store->name }}</a></td>
                            <td>{{ $approvedorders->order_status == 1 ? 'Ready' : 'Unprepared' }}</td>
                            <td>{{ $approvedorders->payment_status == 1 ? 'Approved' : 'Unapproved' }}</td>
                            <td>{{ $approvedorders->created_at }}</td>
                          
                        </tr>
                        @endforeach
         
                    </tbody>
                </table>
               @endif
            </div>
       
       </div>
    </div><!-- /.container-fluid -->
  </div>


@endsection
     