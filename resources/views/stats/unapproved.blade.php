@extends('layouts.layout')

@section('content')


    <div class="container">
      <div class="row">
        <div class="col-md- automargin">
          <h2 class="p-3">All Unapproved Orders</h2>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  <!-- /.content-header -->

  <!-- Main content -->

       <div class="container">
            @if(isset($unapproved))
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Reference Number</th>
                            <th>Customer Name</th>
                            <th>Customer Email</th>
                            <th>Store Name</th>
                            <th>Order Status</th>
                            <th>Payment Status</th>
                            <th>Booking Date/Time</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($unapproved as $unapprovedorders)
                        <tr>
                            <td><a href="/orders/{{ $unapprovedorders->id }}">{{ $unapprovedorders->reference_num }}</td>
                            <td><a href="/customers/{{ $unapprovedorders->customer->id }}">{{ $unapprovedorders->customer->name }}</a></td>
                            <td>{{ $unapprovedorders->customer->email }}</td>
                            <td><a href="/stores/{{ $unapprovedorders->store->id }}">{{ $unapprovedorders->store->name }}</a></td>
                            <td>{{ $unapprovedorders->order_status == 1 ? 'Ready' : 'Unprepared' }}</td>
                            <td>{{ $unapprovedorders->payment_status == 1 ? 'Approved' : 'Unapproved' }}</td>
                            <td>{{ $unapprovedorders->created_at }}</td>
                          
                        </tr>
                        @endforeach
         
                    </tbody>
                </table>
               @endif
            </div>
       
       </div>
    </div><!-- /.container-fluid -->
  </div>


@endsection
     