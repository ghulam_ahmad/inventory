@extends('layouts.layout')

@section('content')


    <div class="container">
      <div class="row">
        <div class="col-md- automargin">
          <h2 class="p-3">All Ready Orders</h2>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  <!-- /.content-header -->

  <!-- Main content -->

       <div class="container">
            @if(isset($ready))
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Reference Number</th>
                            <th>Customer Name</th>
                            <th>Customer Email</th>
                            <th>Store Name</th>
                            <th>Order Status</th>
                            <th>Payment Status</th>
                            <th>Booking Date/Time</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ready as $readyorders)
                        <tr>
                            <td><a href="/orders/{{ $readyorders->id }}">{{ $readyorders->reference_num }}</td>
                            <td><a href="/customers/{{ $readyorders->customer->id }}">{{ $readyorders->customer->name }}</a></td>
                            <td>{{ $readyorders->customer->email }}</td>
                            <td><a href="/stores/{{ $readyorders->store->id }}">{{ $readyorders->store->name }}</a></td>
                            <td>{{ $readyorders->order_status == 1 ? 'Ready' : 'Unprepared' }}</td>
                            <td>{{ $readyorders->payment_status == 1 ? 'Approved' : 'Unapproved' }}</td>
                            <td>{{ $readyorders->created_at }}</td>
                          
                        </tr>
                        @endforeach
         
                    </tbody>
                </table>
               @endif
            </div>
       
       </div>
    </div><!-- /.container-fluid -->
  </div>


@endsection
     