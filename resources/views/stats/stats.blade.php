@extends('layouts.layout')

@section('content')
<div class="container">
	<div class="row">
		<h1 class="center">Dashboard</h1>
	</div>
</div>

      <div class="container-fluid padding">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{ $products }}</h3>

                <p>Total Products</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="/products" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

                    <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ $orders }}</h3>

                <p>Total Orders</p>
              </div>
             <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="/orders" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

                    <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{ $customers }}</h3>

                <p>Total Customers</p>
              </div>
             <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="/customers" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

                    <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{ $stores }}</h3>

                <p>Total Stores</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="/stores" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          
        </div>

        <div class="row">
             <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <h3>{{ $ready_orders }}</h3>

                <p>Ready Orders</p>
              </div>
              <div class="icon">
                <i class="far fa-check-circle"></i>
              </div>
              <a href="/ready" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

             <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{ $unprepared_orders }}</h3>

                <p>Unprepared Orders</p>
              </div>
              <div class="icon">
                <i class="fas fa-times"></i>
              </div>
              <a href="/unprepared" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ $approved_orders }}</h3>

                <p>Approved Orders</p>
              </div>
              <div class="icon">
                <i class="far fa-check-circle"></i>
              </div>
              <a href="/approved" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{ $unapproved_orders }}</h3>

                <p>Unapproved Orders</p>
              </div>
              <div class="icon">
                <i class="fas fa-times"></i>
              </div>
              <a href="/unapproved" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

        </div>
       </div>

@endsection