@extends('layouts.layout')

@section('content')
<div class="container" style="padding-top: 20px; padding-bottom: 20px;">
     <h1>New Customer</h1>
     <form method="POST" action="/customers">
          @csrf
          <div class="form-group">
            <label for="exampleFormControlInput1">Customer Name</label>
            <input value="{{ old('name') }}" style="@error('name') border: 2px solid #ff3860; @enderror"  type="text" name="name" class="form-control" id="exampleFormControlInput1" placeholder="Enter Customer Name">
            @error('name')
            <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('name') }}</p>
            @enderror

          </div>

             <div class="form-group">
            <label for="exampleFormControlInput1">Customer Email</label>
            <input value="{{ old('email') }}" style="@error('email') border: 2px solid #ff3860; @enderror"  type="text" name="email" class="form-control" id="exampleFormControlInput1" placeholder="Enter Customer Email">
            @error('email')
            <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('email') }}</p>
            @enderror
          </div>

        <div class="form-group">
            <label for="exampleFormControlInput1">Customer Address</label>
            <input value="{{ old('Address') }}" style="@error('address') border: 2px solid #ff3860; @enderror"  type="text" name="Address" class="form-control" id="exampleFormControlInput1" placeholder="Enter Customer Address">
            @error('Address')
            <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('Address') }}</p>
            @enderror

          </div>

             <div class="form-group">
            <label for="exampleFormControlInput1">Customer Phone</label>
            <input value="{{ old('phone') }}" style="@error('phone') border: 2px solid #ff3860; @enderror"  type="text" name="phone" class="form-control" id="exampleFormControlInput1" placeholder="Enter Customer Phone">
            @error('phone')
            <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('phone') }}</p>
            @enderror

          </div>

             <div class="form-group">
            <label for="exampleFormControlInput1">Customer Mobile</label>
            <input value="{{ old('mobile') }}" style="@error('mobile') border: 2px solid #ff3860; @enderror"  type="text" name="mobile" class="form-control" id="exampleFormControlInput1" placeholder="Enter Store Name">
            @error('mobile')
            <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('mobile') }}</p>
            @enderror

          </div>
          
          <div>
               <button type="submit" class="btn btn-primary">Submit</button>
             </div>
        </form>
</div>
@endsection