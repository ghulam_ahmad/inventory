@extends('layouts.layout')

@section('content')
<div class="container" style="padding-top: 20px; padding-bottom: 20px;">
     <h1>Edit Customer</h1>
     <form method="POST" action="/customers/{{ $customer->id }}">
          @csrf
          @method('PUT')
          <div class="form-group">
            <label for="exampleFormControlInput1">Customer Name</label>
          <input style="@error('name') border: 2px solid #ff3860; @enderror" type="text" name="name" class="form-control" id="exampleFormControlInput1" placeholder="Enter Customer Name" value="{{ $customer->name }}">
          @error('name')
            <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('name') }}</p>
            @enderror 
          
          </div>
          
                    <div class="form-group">
            <label for="exampleFormControlInput1">Customer Email</label>
          <input style="@error('email') border: 2px solid #ff3860; @enderror" type="text" name="email" class="form-control" id="exampleFormControlInput1" placeholder="Enter Customer Email" value="{{ $customer->email }}">
          @error('email')
            <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('email') }}</p>
            @enderror 
          
          </div>
                              <div class="form-group">
            <label for="exampleFormControlInput1">Customer Address</label>
          <input style="@error('Address') border: 2px solid #ff3860; @enderror" type="text" name="Address" class="form-control" id="exampleFormControlInput1" placeholder="Enter Customer Address" value="{{ $customer->Address }}">
          @error('Address')
            <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('Address') }}</p>
            @enderror 
          
          </div>

                    <div class="form-group">
            <label for="exampleFormControlInput1">Customer Phone</label>
          <input style="@error('phone') border: 2px solid #ff3860; @enderror" type="text" name="phone" class="form-control" id="exampleFormControlInput1" placeholder="Enter Customer Phone" value="{{ $customer->phone }}">
          @error('phone')
            <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('phone') }}</p>
            @enderror 
          
          </div>

                    <div class="form-group">
            <label for="exampleFormControlInput1">Customer Mobile</label>
          <input style="@error('mobile') border: 2px solid #ff3860; @enderror" type="text" name="mobile" class="form-control" id="exampleFormControlInput1" placeholder="Enter Customer Mobile" value="{{ $customer->mobile }}">
          @error('mobile')
            <p style="color: #ff3860;; font-size: 12px; padding: 5px;">{{ $errors->first('mobile') }}</p>
            @enderror 
          
          </div>

          <div>
               <button type="submit" class="btn btn-primary">Submit</button>
             </div>
        </form>
</div>
@endsection