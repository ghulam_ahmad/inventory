@extends('layouts.layout')

@section('content')

<style>
     .img img {
      width: 100px;
      display: block;
      height: auto;
     }
     .center{
      padding: 20px;
     }

     .right{
      float: right;
     }
     .no-decoration:hover{
        text-decoration: none;
     }
     .automargin {
        margin: auto;
     }

</style>


    <div class="container">
      <div class="row">
        <div class="col-md-6 automargin">
          <h2><a class="no-decoration" href="/customers">Customers</a></h2>
        </div><!-- /.col -->
        <div class="col-md-6 center">
            <a href="/customers/create"><button type="button" class="btn btn-primary right">Add New Customer</button></a>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->

   <div class="container">
    <form action="/customers" method="GET" role="search">
        <div class="input-group">
            <input type="text" class="form-control" name="search"
                placeholder="Search Customer"> <span class="input-group-btn">
                <button type="submit" class="btn btn-primary">
                    <span class="glyphicon glyphicon-search">Search</span>
                </button>
            </span>
        </div>
    </form>
    <br>
</div>
<div class="container">
     @if(isset($customers))
    
         <table class="table table-hover">
             <thead>
                 <tr>
                     <th>Customer Name</th>
                     <th>Customer Email</th>
                     <th>Customer Phone</th>
                     <th>Customer Mobile</th>
                     <th></th>
                     <th></th>
                 </tr>
             </thead>
             <tbody>
                 @foreach($customers as $customer)
                 <tr>
                     <td><a href="/customers/{{ $customer->id }}">{{$customer->name}}</a></td>
                     <td>{{ $customer->email }}</td>
                     <td>{{ $customer->phone }}</td>
                     <td>{{ $customer->mobile }}</td>
                     <td style="float: right;" ><a href="delete/customers/{{ $customer->id }}"><button type="button" class="btn btn-danger"><i class="fas fa-trash"></i></button></a></td>
                     <td style="float: right;" ><a href="/customers/{{ $customer->id }}/edit"><button type="button" class="btn btn-primary"><i class="fas fa-edit"></i></button></a></td>
                    </tr>
                 @endforeach
  
             </tbody>
         </table>
         @endif
     </div>

@endsection
     