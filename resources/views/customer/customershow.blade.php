
@extends('layouts.layout')

@section('content')

    <div class="container">
      <div class="row">
        <div style="padding-top: 20px; " class="col-md-6 automargin">
          <h2>Customer Information</h2>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->


<div class="container">
  <div class="row">
    <div class="right">
        <div id="content" style="padding: 20px;">
               <div style="float: right;">
                    <a href="/customers"><button type="button" class="btn btn-primary">Show All Customers</button></a>
                    <a href="/customers/{{ $customer->id }}/edit"><button type="button" class="btn btn-primary">Edit Customer</button></a>
               </div>
       </div>
    </div>
  </div>
</div>


  <div id="page" class="container">
         <div class="row" style="padding-top: 30px">
              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="far fa-user"></i></span>

                     <div class="info-box-content">
                      <span class="info-box-text">Customer Name</span>
                      <span class="info-box-number">
                          {{ $customer->name }}
                      </span>
                    </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>

                   <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="fas fa-at"></i></span>

                     <div class="info-box-content">
                      <span class="info-box-text">Customer Email</span>
                      <span class="info-box-number">
                            {{ $customer->email }}
                      </span>
                    </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>

              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="fas fa-phone"></i></span>

                     <div class="info-box-content">
                      <span class="info-box-text">Customer Phone</span>
                      <span class="info-box-number">
                          {{ $customer->phone }}
                      </span>
                    </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>

              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1"><i class="fas fa-mobile"></i></span>

                     <div class="info-box-content">
                      <span class="info-box-text">Customer Mobile</span>
                      <span class="info-box-number">
                          {{ $customer->mobile }}
                      </span>
                    </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
            </div>

            <div class="row">
              <div style="padding: 20px">
                <label>Customer Address</label>
                <h4>{{ $customer->Address }}</h4>
              </div>
            </div>

              @if(isset($customer->orders))
          <h4 class="p-2">Orders by {{ $customer->name }}</h4>
              <table class="table table-hover">
                  <thead>
                      <tr>
                          <th>Reference Number</th>
                          <th>Order Status</th>
                          <th>Payment Status</th>
                          <th>Order Date/Time</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach($customer->orders as $order)
                      <tr>
                          <td><a href="/orders/{{ $order->id }}">{{ $order->reference_num }}</a></td>
                          <td>{{ $order->order_status == 1 ? 'Ready' : 'Unprepared' }}</td>
                          <td>{{ $order->payment_status == 1 ? 'Approved' : 'Not Approved' }}</td>
                          <td>{{ $order->created_at }}</td>
                      </tr>
                      @endforeach

                  </tbody>
              </table>
           @endif


	</div>
    
@endsection