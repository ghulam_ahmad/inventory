<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
     return redirect('/login');
});

Route::get('/vue', 'CustomerController@vue');

Route::get('/home', function () {
    return redirect('/products');
});

Route::get('/php', 'CustomerController@hello');

Auth::routes();

Route::group(array('middleware' => 'auth'), function () {

// product routes

Route::post('/products', 'ProductController@store');

Route::get('/products/create', 'ProductController@create');

Route::get('/products/fetch', 'ProductController@fetch');

Route::get('/products/{product}', 'ProductController@show');

Route::get('/products/{product}/edit', 'ProductController@edit');

Route::put('/products/{id}', 'ProductController@update');

Route::get('/allproducts', 'ProductController@loadtable');

Route::get('/products', 'ProductController@index');

Route::get('delete/products/{id}', 'ProductController@destroy');

// Store Routes

Route::post('/stores', 'StoreController@store');

Route::get('/stores/create', 'StoreController@create');

Route::get('/stores/fetch', 'StoreController@fetchstores');

Route::get('/stores/{store}', 'StoreController@show');

Route::get('/stores/{store}/edit', 'StoreController@edit');

Route::put('/stores/{id}', 'StoreController@update');

Route::get( '/stores', 'StoreController@index');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('delete/stores/{id}', 'StoreController@destroy');

// Order Routes

Route::post('/orders/store', 'OrderController@store');

Route::get('/orders/create', 'OrderController@create');

Route::get('/orders/search', 'OrderController@search');

Route::get('/orders/{order}', 'OrderController@show');

Route::get('/orders/{order}/edit', 'OrderController@edit');

Route::put('/orders/{id}', 'OrderController@update');

Route::get( '/orders', 'OrderController@index');

Route::get('delete/orders/{id}', 'OrderController@destroy');

// Customer Routes

Route::post('/customers', 'CustomerController@store');

Route::get('/customers/create', 'CustomerController@create');

Route::get('/customers/fetch', 'CustomerController@fetchit');

Route::get('/customers/{customer}', 'CustomerController@show');

Route::get('/customers/{customer}/edit', 'CustomerController@edit');

Route::put('/customers/{id}', 'CustomerController@update');

Route::get( '/customers', 'CustomerController@index');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('delete/customers/{id}', 'CustomerController@destroy');

// Stats Routes

Route::get('/stats', 'StatsController@index');

Route::get('/ready', 'statsController@ready');

Route::get('/unprepared', 'statsController@unprepared');

Route::get('/approved', 'statsController@approved');

Route::get('/unapproved', 'statsController@unapproved');

});
