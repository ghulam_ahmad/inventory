<?php

namespace App;

use App\Product;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['name', 'address', 'payment', 'delivery_note', 'reference_num', 'order_status', 'product_id'];

    public function products()
	{
		return $this->belongsToMany('App\Product')->withPivot('quantity', 'cost')->withTimestamps();;
	}

        public function store() {
        	return $this->belongsTo(Store::class);
        }

        public function customer()
		{
		   return $this->belongsTo('App\Customer');
		}

}
