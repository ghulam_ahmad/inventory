<?php

namespace App;

use Spatie\MediaLibrary\Models\Media;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use App\Order;

class Product extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = ['product_code', 'product_name', 'cost', 'stock', 'description'];

    protected $guarded = [];

    public function stores(){
        return $this->belongsToMany(Store::class)->withPivot('code')->withTimestamps();
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
              ->width(200)
              ->height(200)
              ->sharpen(10);

        $this->addMediaConversion('square')
              ->width(412)
              ->height(412)
              ->sharpen(10);
    }

      public function orders()
      {
         return $this->belongsToMany('App\Order')->withPivot('quantity', 'cost')->withTimestamps();
      }

}
