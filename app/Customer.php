<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    
    protected $fillable = ['name', 'email', 'Address', 'phone', 'mobile'];

    protected $guarded = [];

    public function orders()
{
   return $this->hasMany('App\Order');
}
}
