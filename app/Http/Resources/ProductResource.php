<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
           'id' => $this->id,
           'Product Code' => $this->product_code,
           'Product Name' => $this->product_name,
           'Description' => $this->description,
           'Cost' => $this->cost,
           'Stock' => $this->stock,
        ];
    }
}
