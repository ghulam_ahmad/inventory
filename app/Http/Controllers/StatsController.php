<?php

namespace App\Http\Controllers;

use App\Order;
use App\Store;
use App\Product;
use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatsController extends Controller
{
    public function index()
    {
    	$products = Product::all()->count();
    	$ready_orders = DB::table('orders')->where('order_status', '1')->count();
    	$unprepared_orders = DB::table('orders')->where('order_status', '0')->count();
        $approved_orders = DB::table('orders')->where('payment_status', '1')->count();
        $unapproved_orders = DB::table('orders')->where('payment_status', '0')->count();
    	$orders = Order::all()->count();
    	$customers = Customer::all()->count();
    	$stores = Store::all()->count();
    	return view ( 'stats.stats' , compact('products', 'ready_orders', 'approved_orders', 'unapproved_orders', 'unprepared_orders', 'orders', 'customers', 'stores'));
    }

    public function ready() {
    	$ready = Order::get()->where('order_status', '1');
    	return view ('stats.allready', compact('ready'));
    }

    public function unprepared() {
    	$unprepared = Order::get()->where('order_status', '0');
    	return view ('stats.allunprepared', compact('unprepared'));
    }

    public function approved() {
        $approved = Order::get()->where('payment_status', '1');
        return view ('stats.allapproved', compact('approved'));
    }

        public function unapproved() {
        $unapproved = Order::get()->where('payment_status', '0');
        return view ('stats.unapproved', compact('unapproved'));
    }

    
}
