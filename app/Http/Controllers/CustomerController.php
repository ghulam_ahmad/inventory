<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Product;
use Illuminate\Http\Request;

class CustomerController extends Controller
{

    public function index(Request $request)
    {
        $searchtext = $request->search;
        $customers = Customer::where ( 'name', 'LIKE', '%' . $searchtext . '%' )->orWhere ( 'phone', 'LIKE', '%' . $searchtext . '%' )->latest()->paginate(25);
        return view ( 'customer.customers' , compact('customers'));

    }

public function fetchit(Request $request)
    {
        $searchtext = $request->search;
        if(strlen($searchtext) > 0){
            $customers = Customer::where ( 'name', 'LIKE', '%' . $searchtext . '%' )->orWhere ( 'email', 'LIKE', '%' . $searchtext . '%' )->selectRaw("name as label, id as code")->limit(30)->get();
            return response()->json($customers);
        }
        return false;
    }

    public function anything(Request $request)
    {
        dd('hello world');
    }

    public function create()
    {
        $customers = Customer::all();
        return view('customer.createcustomer', compact('customers'));
    }


    public function store(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'Address' => 'required',
        ]);
       $customer = Customer::create($request->all());

        return redirect('/customers');
    }


    public function show(Customer $customer)
    {
        return view('customer.customershow', compact('customer'));

    }


    public function edit(Customer $customer)
    {   
        return view('customer.editcustomer', compact('customer'));
    }


    public function update(Request $request, $id)
    {
       $validate =  $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'Address' => 'required',
        ]);

        $customer = Customer::find($id);

        $customer->name = request('name');
        $customer->email = request('email');
        $customer->phone = request('phone');
        $customer->Address = request('Address');
        $customer->mobile = request('mobile');

        $customer->save();

        return redirect('/customers');
    }

    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        $customer->delete();

        return redirect('/customers');
    }
}
