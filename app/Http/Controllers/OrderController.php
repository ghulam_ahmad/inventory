<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Order;
use App\Product;
use App\Store;
use App\Customer;


class OrderController extends Controller
{
    public function index(Request $request)
    {
        $searchtext = $request->search;
        $orders = Order::where ( 'reference_num', 'LIKE', '%' . $searchtext . '%' )->latest()->paginate(25);
        return view ( 'order.orders' , compact('orders'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::selectRaw("product_name as label, id as code")->get()->toArray();
        $stores = Store::selectRaw("name as label, id as code")->get()->toArray();
        $customers = Customer::selectRaw("name as label, id as code, email as email")->get()->toArray();
        return view('order.createorder', compact('products', 'stores', 'customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        $products = $request->products;
        $order = new Order;
        $order->reference_num = $request->referenceNumber;
        $order->order_status = $request->orderStatus;
        $order->payment_status = $request->paymentStatus;
        $order->delivery_note = $request->deliveryNote;
        $order->customer_id = $request->customer_id;
        $order->store_id = $request->store_id;
        $order->save();
        $sync_array = [];
        foreach ($products as $value) {
           $sync_array[$value['product_id']] = $value;  
        }
        $order->products()->sync($sync_array);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return view('order.ordershow', compact('order'));
    }

    public function search(Request $request)
    {
        $searchtext = $request->search;
        $products = Product::where( 'product_code', 'LIKE', '%' . $searchtext . '%' )->orWhere ( 'product_name', 'LIKE', '%' . $searchtext . '%' )->select("id","product_code","product_name")->limit(10)->latest()->get();
        return response()->json($products);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        return view('order.editorder', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);;
        $products = $request->products;
        $order->reference_num = $request->referenceNumber;
        $order->order_status = $request->orderStatus;
        $order->payment_status = $request->paymentStatus;
        $order->delivery_note = $request->deliveryNote;
        $order->customer_id = $request->customer_id;
        $order->store_id = $request->store_id;
        $sync_array = [];
        foreach ($products as $value) {
           $sync_array[$value['product_id']] = $value;  
        }
        $order->products()->sync($sync_array);
        $order->save();
        return redirect('/orders/' . $order->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        $order->delete();

        return redirect('/orders');
    }
}

