<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use App\Store;

use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function index(Request $request)
    {
        $searchtext = $request->search;
        $stores = Store::where ( 'name', 'LIKE', '%' . $searchtext . '%' )->latest()->paginate(25);
        return view ( 'store.stores' , compact('stores'));

    }

    public function fetchstores(Request $request)
    {
        $searchtext = $request->search;
        if(strlen($searchtext) > 0){
            $stores = Store::where ( 'name', 'LIKE', '%' . $searchtext . '%' )->selectRaw("name as label, id as code")->limit(30)->get();
            return response()->json($stores);
        }
        return false;
    }


    public function create()
    {
        return view('store.createstore');
    }


    public function store(Request $request)
    {

        Store::create(request()->validate([
            'name' => 'required',
        ]));
         
        return redirect('/stores');
    }

    public function show(Store $store)
    {
        return view('store.storesshow', compact('store'));

    }

    public function edit(Store $store)
    {
        return view('store.editstore', compact('store'));
    }


    public function update($id)
    {
        request()->validate([
            'name' => 'required',
        ]);

        $store = Store::find($id);

        $store->name = request('name');

        $store->save();

        return redirect('/stores/' . $store->id);
    }

    public function destroy($id)
    {
        $store = Store::findOrFail($id);
        $store->delete();

        return redirect('/stores');
    }
}
