<?php

namespace App\Http\Controllers;

use App\Store;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Resources\ProductResource;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function loadtable(Request $request)
    {
        $searchtext = $request->search;
        $products = Product::where ( 'product_code', 'LIKE', '%' . $searchtext . '%' )->orWhere ( 'product_name', 'LIKE', '%' . $searchtext . '%' )->latest()->get();
           return response()->json($products);

    }

    public function index()
    {
        return view('product.products');
    }

    public function fetch(Request $request)
    {
        $searchtext = $request->search;
        if(strlen($searchtext) > 0){
            $products = Product::where ( 'product_code', 'LIKE', '%' . $searchtext . '%' )->orWhere ( 'product_name', 'LIKE', '%' . $searchtext . '%' )->selectRaw("product_name as label, id as code, product_code as product_code, cost as cost, stock as stock")->limit(30)->get();
            return response()->json($products);
        }
        return false;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stores = Store::all();
        return view('product.createproduct', compact('stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validate = $request->validate([
            'product_code' => 'required',
            'product_name' => 'required',
            'stock' => 'required',
            'cost' => 'required',
            'description' => 'required',
        ]);

       $product = Product::create($request->except(['images']));

       $stores = Store::get()->pluck('id');

       foreach ($stores as $key => $value) {

        $code = $request['code-'.$value];
        
        $product->stores()->attach($value, ['code' => $code]);

        }
        
        foreach ($request->images as $image) {
            $product->addMedia($image)->toMediaCollection('images');
        }

        return redirect('/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('product.productsshow', compact('product'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {   
        $stores = Store::all();
        return view('product.editproducts', compact('product', 'stores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $validate =  $request->validate([
            'product_code' => 'required',
            'product_name' => 'required',
            'stock' => 'required',
            'cost' => 'required',
            'description' => 'required',
        ]);

        $product = Product::find($id);

        $product->product_code = request('product_code');
        $product->product_name = request('product_name');
        $product->stock = request('stock');
        $product->cost = request('cost');
        $product->description = request('description');

        if($request->hasFile('image')){
            $product->addMediaFromRequest('image')->toMediaCollection('images');
        }
        $stores = Store::get()->pluck('id');

       foreach ($stores as $store) {
            $code = $store['code'];
        }

        $product->save();

        return redirect('/products/' . $product->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        return redirect('/products');
    }
}
