<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
	protected $fillable = ['name'];

    protected $guarded = [];

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('code')->withTimestamps();
    }

    public function orders() {
    	return $this->hasMany(Order::class);
    }
}
