# ************************************************************
# Sequel Pro SQL dump
# Version 5446
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 8.0.19)
# Database: inventory
# Generation Time: 2020-06-15 14:41:21 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table customers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;

INSERT INTO `customers` (`id`, `name`, `email`, `phone`, `Address`, `mobile`, `created_at`, `updated_at`)
VALUES
	(2,'cuvep','hyroseqas@mailinator.com','921345345345','ryzuxovic B block Lahore Pakistan','123123131233123','2020-05-04 18:20:34','2020-05-04 18:20:34'),
	(3,'Mudassar Gondal','qije@mailinator.com','123123123123','123 street 123 block shalimar colony Lahore','2479274987984','2020-05-04 23:51:21','2020-05-06 16:13:27');

/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table failed_jobs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table media
# ------------------------------------------------------------

DROP TABLE IF EXISTS `media`;

CREATE TABLE `media` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` bigint unsigned NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `responsive_images` json NOT NULL,
  `order_column` int unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;

INSERT INTO `media` (`id`, `model_type`, `model_id`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`)
VALUES
	(1,'App\\Product',51,'images','black','black.jpg','image/jpeg','public',213779,'[]','{\"generated_conversions\": {\"thumb\": true, \"square\": true}}','[]',1,'2020-05-05 00:59:39','2020-05-05 00:59:40');

/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2019_08_19_000000_create_failed_jobs_table',1),
	(4,'2020_03_04_151021_create_products_table',1),
	(5,'2020_03_06_134003_create_stores_table',1),
	(6,'2020_03_11_144757_create_product_store_table',1),
	(7,'2020_03_13_140254_create_orders_table',1),
	(8,'2020_03_16_140426_create_media_table',1),
	(9,'2020_03_30_133020_create_order_product_table',1),
	(10,'2020_04_03_141220_create_customers_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table order_product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `order_product`;

CREATE TABLE `order_product` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint unsigned NOT NULL,
  `product_id` bigint unsigned DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `cost` decimal(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `order_product` WRITE;
/*!40000 ALTER TABLE `order_product` DISABLE KEYS */;

INSERT INTO `order_product` (`id`, `order_id`, `product_id`, `quantity`, `cost`, `created_at`, `updated_at`)
VALUES
	(41,25,3,12,19.00,NULL,NULL),
	(42,25,5,32,80.00,NULL,NULL),
	(43,26,6,23,50.00,NULL,NULL),
	(44,26,5,43,80.00,NULL,NULL),
	(45,27,5,12,80.00,'2020-06-14 21:44:53','2020-06-14 21:44:53'),
	(46,27,13,12,58.00,'2020-06-14 21:44:53','2020-06-14 21:44:53'),
	(48,30,40,12,94.00,'2020-06-14 22:41:02','2020-06-14 22:41:02'),
	(49,30,1,33,3.00,'2020-06-14 22:41:02','2020-06-14 22:41:02'),
	(50,30,52,5,500.44,'2020-06-14 22:41:02','2020-06-14 22:41:02');

/*!40000 ALTER TABLE `order_product` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int DEFAULT NULL,
  `store_id` int DEFAULT NULL,
  `reference_num` int NOT NULL,
  `delivery_note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_status` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;

INSERT INTO `orders` (`id`, `customer_id`, `store_id`, `reference_num`, `delivery_note`, `order_status`, `created_at`, `updated_at`)
VALUES
	(27,3,2,122,'1111',1,'2020-06-14 21:44:53','2020-06-14 21:44:53'),
	(28,3,2,2222,'222',0,'2020-06-14 21:45:16','2020-06-14 21:45:16'),
	(30,3,3,3453434,'hello world',1,'2020-06-14 22:41:02','2020-06-14 22:41:02');

/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table product_store
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_store`;

CREATE TABLE `product_store` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `store_id` bigint unsigned NOT NULL,
  `product_id` bigint unsigned NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `product_store` WRITE;
/*!40000 ALTER TABLE `product_store` DISABLE KEYS */;

INSERT INTO `product_store` (`id`, `store_id`, `product_id`, `code`, `created_at`, `updated_at`)
VALUES
	(1,1,51,'Cora Tillman','2020-05-05 00:59:39','2020-05-05 00:59:39'),
	(2,2,51,'Ingrid Velazquez','2020-05-05 00:59:39','2020-05-05 00:59:39'),
	(3,3,51,'Justine Diaz','2020-05-05 00:59:39','2020-05-05 00:59:39'),
	(28,1,52,'Code 1','2020-05-19 20:43:51','2020-05-19 20:43:51'),
	(29,2,52,'Code 2','2020-05-19 20:43:51','2020-05-19 20:43:51'),
	(30,3,52,'Code 3','2020-05-19 20:43:51','2020-05-19 20:43:51');

/*!40000 ALTER TABLE `product_store` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `stock` int unsigned NOT NULL,
  `cost` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_stock_index` (`stock`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;

INSERT INTO `products` (`id`, `product_code`, `product_name`, `description`, `stock`, `cost`, `created_at`, `updated_at`)
VALUES
	(1,'aperiam','et enim','Quae illum doloribus eius hic aperiam.',63,3.00,'2019-11-09 20:03:52','2020-01-23 20:39:22'),
	(2,'modi','totam rerum','Ullam ad iste natus repellendus nulla fugiat.',54,55.00,'2020-04-23 12:28:38','2020-02-09 18:29:34'),
	(3,'voluptatem','quibusdam unde','Numquam dolor earum quas reiciendis.',92,19.00,'2019-09-23 11:56:08','2020-04-25 07:09:51'),
	(5,'magni','sed occaecati','Quasi et et ullam et excepturi et cumque.',92,80.00,'2020-03-30 02:22:04','2020-02-22 17:55:18'),
	(6,'quis','laudantium ratione','Tempore velit magni molestiae officia.',78,50.00,'2019-07-07 23:22:17','2020-02-03 02:23:13'),
	(7,'magnam','temporibus qui','Nisi ut mollitia est beatae.',46,57.00,'2019-07-21 19:35:50','2019-12-30 02:11:52'),
	(8,'consequatur','dolorem labore','Amet dolorem hic praesentium omnis.',62,29.00,'2020-04-15 07:13:44','2019-12-28 10:55:03'),
	(9,'est','ut quas','Rerum veritatis unde voluptas minima.',63,36.00,'2019-10-04 02:23:37','2020-02-08 10:24:17'),
	(10,'est','voluptatem cumque','Eum similique error velit ut asperiores.',84,4.00,'2019-12-03 08:23:45','2020-01-28 01:34:24'),
	(11,'officia','eveniet occaecati','Reiciendis qui non quidem.',14,42.00,'2020-03-03 00:41:40','2020-04-02 16:11:42'),
	(12,'iure','consectetur ut','Eum non animi beatae ad nihil voluptas.',49,5.00,'2020-01-29 02:59:59','2020-01-04 08:27:43'),
	(13,'et','est voluptates','Impedit velit vel quidem possimus recusandae.',0,58.00,'2020-01-27 19:54:12','2019-12-16 09:20:15'),
	(14,'ab','voluptatum accusamus','Voluptas velit ad nesciunt qui soluta.',57,90.00,'2019-09-19 12:37:20','2020-03-16 06:26:34'),
	(15,'vero','quidem in','Quos nesciunt suscipit ipsum voluptas ducimus.',72,34.00,'2020-04-08 01:09:39','2019-12-06 21:44:55'),
	(16,'nobis','inventore vel','Dolor nobis maxime qui pariatur.',30,77.00,'2019-09-03 18:58:53','2020-02-17 12:16:39'),
	(17,'consequatur','perspiciatis quae','In cumque laborum est doloribus qui tempore.',93,72.00,'2020-03-01 11:39:44','2020-01-12 06:37:12'),
	(18,'nobis','delectus sint','Ad veniam quod ut quam et nihil.',8,62.00,'2020-03-14 17:01:01','2020-04-12 00:15:18'),
	(19,'illo','sit recusandae','Vitae facere qui enim quod ipsam nihil.',35,3.00,'2019-10-25 08:12:24','2020-01-01 06:32:38'),
	(20,'velit','qui nesciunt','Praesentium necessitatibus quia est et est amet.',19,32.00,'2019-11-02 10:41:36','2020-04-22 21:50:09'),
	(21,'eum','aut libero','Architecto maxime adipisci animi rem.',60,37.00,'2019-10-26 19:05:18','2020-02-12 15:27:45'),
	(22,'nesciunt','sed qui','Natus omnis provident illo.',20,85.00,'2019-07-30 22:50:27','2020-01-15 21:46:47'),
	(23,'dolores','ab odio','Necessitatibus libero non eveniet.',56,42.00,'2019-09-30 05:47:43','2020-04-10 00:04:00'),
	(24,'quo','blanditiis numquam','Dolorem magnam iste incidunt.',34,46.00,'2019-12-14 04:46:32','2020-02-18 09:27:39'),
	(25,'earum','voluptates distinctio','Cum quod quae voluptatem doloribus optio debitis.',74,53.00,'2020-04-06 22:22:07','2020-01-14 04:02:05'),
	(26,'laudantium','consequuntur autem','Porro vel quam voluptas culpa tempore qui.',53,88.00,'2019-10-21 03:59:49','2020-03-23 19:30:25'),
	(27,'provident','est officiis','Dolores aut minima cupiditate hic.',56,7.00,'2019-09-15 23:04:50','2019-12-20 03:58:18'),
	(28,'esse','voluptate nostrum','Delectus pariatur vero rerum autem.',13,69.00,'2019-12-09 13:55:01','2020-01-13 17:54:30'),
	(29,'est','enim ad','Accusamus eos tempora rerum quia.',36,97.00,'2019-08-06 00:47:01','2020-03-17 09:41:49'),
	(30,'cumque','repellat autem','Et tempora ipsum ad amet veritatis sint autem.',59,50.00,'2020-04-12 03:58:31','2020-03-14 01:18:10'),
	(31,'et','eveniet sed','Unde omnis ab sequi est.',19,63.00,'2019-07-15 00:35:58','2019-12-21 07:39:31'),
	(32,'voluptates','cupiditate et','Ea ipsam recusandae consequatur eos ut officiis.',77,54.00,'2020-01-26 20:25:07','2020-01-05 21:29:56'),
	(33,'totam','ut animi','Suscipit dignissimos accusantium itaque aut.',8,72.00,'2020-01-27 11:18:59','2020-01-11 19:27:04'),
	(34,'similique','temporibus enim','Quibusdam distinctio qui et omnis.',56,85.00,'2019-12-07 14:40:08','2020-01-05 20:23:38'),
	(35,'dolore','soluta magni','Dolore reiciendis recusandae a.',40,69.00,'2019-07-29 03:34:25','2020-04-13 02:50:02'),
	(36,'non','facilis amet','Aut dolorum praesentium doloribus id et.',5,32.00,'2019-12-18 00:55:27','2020-04-20 09:03:25'),
	(37,'mollitia','illo quia','Adipisci voluptatem temporibus tempora dolorem.',91,10.00,'2019-11-10 21:48:26','2020-03-20 22:15:55'),
	(38,'veritatis','accusantium earum','Deserunt eos porro dignissimos dolorem.',93,85.00,'2020-03-14 12:31:34','2020-02-01 01:52:52'),
	(39,'quaerat','dolorem aut','Ullam autem velit laboriosam debitis accusantium.',69,15.00,'2019-09-09 16:29:47','2020-01-30 21:37:48'),
	(40,'error','fugit et','Illum explicabo voluptates quae ipsam eaque qui.',75,94.00,'2019-09-10 10:41:06','2019-12-21 00:16:58'),
	(41,'maxime','sapiente magnam','Enim alias odit eligendi soluta quidem dolor.',0,19.00,'2019-10-22 03:35:23','2020-04-20 15:07:33'),
	(42,'corrupti','mollitia ipsam','Et ullam ipsa qui repellendus minus.',52,94.00,'2019-11-29 05:09:24','2020-04-20 11:40:40'),
	(43,'qui','ipsam impedit','Dolor autem et sequi eius reiciendis.',19,74.00,'2019-12-01 02:46:01','2020-04-21 06:29:40'),
	(44,'sed','est a','A molestias omnis possimus.',69,93.00,'2019-08-28 12:06:54','2020-02-29 10:58:24'),
	(45,'voluptatem','provident facere','Nobis fuga cumque odio sed.',78,65.00,'2019-10-20 20:08:55','2020-03-04 15:46:18'),
	(46,'voluptas','saepe aut','Quia nemo aut quis deserunt.',12,37.00,'2019-09-12 03:11:30','2020-02-10 05:29:19'),
	(47,'quia','ea voluptas','Quasi ducimus qui reprehenderit.',97,0.00,'2020-02-19 10:36:46','2020-04-23 16:57:26'),
	(48,'nulla','qui optio','Ut qui ut est blanditiis.',31,87.00,'2020-01-22 17:37:32','2020-04-24 18:42:31'),
	(49,'totam','architecto iste','Est sunt ut est ut incidunt.',50,85.00,'2019-08-17 11:52:49','2020-05-03 21:25:28'),
	(50,'voluptatem','ullam aspernatur','Illum et porro sit asperiores animi.',29,68.00,'2019-10-28 06:12:10','2020-02-14 14:08:30'),
	(51,'Ruth Pate','Anthony Langleyhjhjhjh','Architecto consequat',14,96.00,'2020-05-05 00:59:39','2020-05-06 15:56:36'),
	(52,'Hello World','Macbook Pro','hello 123 this is a sample product',13,500.44,'2020-05-19 20:42:56','2020-05-19 15:45:21');

/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table stores
# ------------------------------------------------------------

DROP TABLE IF EXISTS `stores`;

CREATE TABLE `stores` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `stores` WRITE;
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;

INSERT INTO `stores` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'Apple Store','2020-05-04 18:41:23','2020-05-04 18:41:23'),
	(2,'Marvin Weber','2020-05-04 18:41:27','2020-05-04 18:41:27'),
	(3,'Macy Store','2020-05-04 18:41:32','2020-05-04 18:41:32'),
	(5,'Walmart','2020-05-19 15:44:58','2020-05-19 15:44:58'),
	(6,'yfjhgfjhgfjhgf','2020-06-14 21:51:33','2020-06-14 21:51:33');

/*!40000 ALTER TABLE `stores` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'Mudassar Gondal','admin@mail.com',NULL,'$2y$10$rzvlpdTOCn4LTi9VRt/yZ.Ttdxn5VTR9/Jtgoe.qHQ4yCDnHdu/hW','KCygFLydftqQp8Ib7jMhSFnDTSqQeQncQrJjpuklyb79zeQ39CZMNL5ld3ej','2020-05-04 18:11:21','2020-05-04 18:11:21');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
